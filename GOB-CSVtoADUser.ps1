﻿function GOB-CSVtoADUser
{
    <#
    ...
    #>
    [CmdletBinding(HelpUri='https://github.com/LesOrcsGithub/GOB-CSVtoADUser')]
    Param(
    [Parameter(Mandatory=$false)][string]$CsvPath="D:\XBOK\_Powershell\GOB-CSVtoADUser\ADGOB_utf8_group.csv",
    [Parameter(Mandatory=$false)][bool]$CsvSort=$false,
    [Parameter(Mandatory=$false)][string]$OUGroup="GROUPE-TEST",
    [Parameter(Mandatory=$false)][string]$OUPath="OU=GOBELINS",
    [Parameter(Mandatory=$false)][string]$XboxPath="\\Ad-orc-01\xbok\",
    [Parameter(Mandatory=$false)][string]$AccountPassword="gobelins"
    )

    Get-Variable -Exclude CsvPath,CsvSort,OUGroup,AccountPassword,OUPath,XboxPath | Remove-Variable -ErrorAction SilentlyContinue
    $Error.Clear()
    $Total = $null
    cls

    $Site = "orc.local"
    $Mail = "edu.gobelins.fr"
    $Date = Get-Date -Format "dd/MM/yyyy"

    if ($XboxPath -notlike "*\")
    {
        $XboxPath = $XboxPath + "\"
    }

    foreach ($Split in $Site.Split("."))
    {
        $DC = $DC + ",DC=" + $Split
    }

    # TITLE
    $FnTitle = $MyInvocation.MyCommand.ToString()
    foreach ($FnPath in $CsvPath.Split("\"))
    {
        $FnPath = $FnPath
    }
    $FnTitle = "`n[ " + $fnTitle + " // " + $FnPath + " ]`n"
    Write-Host $FnTitle -ForegroundColor Cyan
    # TITLE

    $ScanCsvPath = Get-Item -Path $CsvPath -ErrorAction SilentlyContinue
    if ($ScanCsvPath -ne $null)
    {
        if ($CsvSort -eq $true)
        {
            $ScanCsv = Import-Csv -Path $CsvPath -Delimiter ";" -Encoding UTF8 | Sort-Object -Property Surname,GivenName
        }
        else
        {
            $ScanCsv = Import-Csv -Path $CsvPath -Delimiter ";" -Encoding UTF8
        }

        foreach ($ADUser in $ScanCsv)
        {
            if ($ADUser.Surname.Length -gt 1 -or $ADUser.GivenName.Length -gt 1)
            {
                # Surname = Surname
                if ($ADUser.Surname.Length -gt 1)
                {
                    $Surname = $ADUser.Surname.ToUpper() -ireplace " ","-" -replace "--","-" -replace "---","-" -replace "----","-" -replace "_","-" -replace ",","" -replace ";","" -replace ":","" -replace [regex]::escape('(') -replace [regex]::escape(')') -replace [regex]::escape('[') -replace [regex]::escape(']') -replace [regex]::escape('{') -replace [regex]::escape('}') -replace [regex]::escape('/')
                
                    if ($Surname -ilike "-*")
                    {
                        $Surname = $Surname.Substring(1)
                    }
                    if ($Surname -ilike "*-")
                    {
                        $Surname = $Surname.Substring(0,$Surname.Length - 1)
                    }
                
                    $iSurname = $Surname.ToLower() -replace "-","" -replace "'","" -ireplace "à","a" -ireplace "â","a" -ireplace "ä","a" -ireplace "é","e" -ireplace "è","e" -ireplace "ê","e" -ireplace "ë","e" -ireplace "î","i" -ireplace "ï","i" -ireplace "ô","o" -ireplace "ö","o" -ireplace "û","u" -ireplace "ü","u" -ireplace "ÿ","y" -ireplace "ç","c" -ireplace "ñ","n"
                }
                else
                {
                    $Surname = $null
                    $iSurname = $null
                }

                # GivenName = GIVENNAME
                if ($ADUser.GivenName.Length -gt 1)
                {
                    $GivenName = $ADUser.GivenName.ToLower() -ireplace " ","-" -replace "--","-" -replace "---","-" -replace "----","-" -replace "_","-" -replace ",","" -replace ";","" -replace ":","" -replace [regex]::escape('(') -replace [regex]::escape(')') -replace [regex]::escape('[') -replace [regex]::escape(']') -replace [regex]::escape('{') -replace [regex]::escape('}') -replace [regex]::escape('/')
                
                    $sGivenName = $GivenName.Split("-")
                    if ($sGivenName.Count -gt 1)
                    {
                        $GivenName = $null
                        foreach ($Split in $sGivenName)
                        {
                            $Split = $Split.Substring(0,1).ToUpper() + $Split.Substring(1)
                            $GivenName = $GivenName + "-" + $Split
                        }
                    }
                    else
                    {
                        $GivenName = $GivenName.Substring(0,1).ToUpper() + $GivenName.Substring(1)
                    }

                    if ($GivenName -ilike "-*")
                    {
                        $GivenName = $GivenName.Substring(1)
                    }
                    if ($GivenName -ilike "*-")
                    {
                        $GivenName = $GivenName.Substring(0,$GivenName.Length - 1)
                    }

                    $iGivenName = $GivenName.ToLower() -replace "-","" -replace "'","" -ireplace "à","a" -ireplace "â","a" -ireplace "ä","a" -ireplace "é","e" -ireplace "è","e" -ireplace "ê","e" -ireplace "ë","e" -ireplace "î","i" -ireplace "ï","i" -ireplace "ô","o" -ireplace "ö","o" -ireplace "û","u" -ireplace "ü","u" -ireplace "ÿ","y" -ireplace "ç","c" -ireplace "ñ","n"
                }
                else
                {
                    $GivenName = $null
                    $iGivenName = $null
                }

                #ALY
                if ($Surname -eq $null -and $GivenName -ne $null)
                {
                    $Surname = $GivenName.ToUpper()
                    $GivenName = $null
                    $iSurname = $iGivenName.ToLower()
                    $iGivenName = $null
                }

                # DisplayName = GIVENNAME Surname
                if ($Surname -ne $null -and $GivenName -ne $null)
                {
                    $DisplayName = $GivenName + " " + $Surname
                }
                elseif ($Surname -ne $null -and $GivenName -eq $null)
                {
                    $DisplayName = $Surname
                }

                # CN/Name = Surname GIVENNAME
                if ($Surname -ne $null -and $GivenName -ne $null)
                {
                    $CName = $Surname + " " + $GivenName
                }
                elseif ($Surname -ne $null -and $GivenName -eq $null)
                {
                    $CName = $Surname
                }

                # EmailAdress = givenname.surname@edu.gobelins.fr
                if ($iSurname -ne $null -and $iGivenName -ne $null)
                {
                    $EmailAdress = $iGivenName + "." + $iSurname + "@" + $Mail
                }
                else
                {
                    $EmailAdress = $null
                }

                # SamAccountName = givenname.surname
                if ($iSurname -ne $null -and $iGivenName -ne $null)
                {
                    if ($iSurname.Length -gt 18)
                    {
                        $iSurname = $iSurname.Substring(0,18)
                    }
                    if ($iSurname.Length + $iGivenName.Length + 1 -gt 20)
                    {
                        $iGivenName = $iGivenName.Substring(0,20 - ($iSurname.Length + 1))
                    }

                    $SamAccountName = $iGivenName + "." + $iSurname
                }
                elseif ($iSurname -ne $null -and $iGivenName -eq $null)
                {
                    if ($iSurname.Length -gt 20)
                    {
                        $iSurname = $iSurname.Substring(0,20)
                    }

                    $SamAccountName = $iSurname
                }

                # PrincipalName = givenname.surname@gobelins.fr
                $PrincipalName = $SamAccountName + "@" + $Site

                # OUGroup = CAAF-2021
                $OUGroup = $OUGroup -ireplace " ","-" -replace "--","-" -replace "---","-" -replace "----","-" -replace "_","-" -replace ",","" -replace ";","" -replace ":","" -replace "'","" -ireplace "à","a" -ireplace "â","a" -ireplace "ä","a" -ireplace "é","e" -ireplace "è","e" -ireplace "ê","e" -ireplace "ë","e" -ireplace "î","i" -ireplace "ï","i" -ireplace "ô","o" -ireplace "ö","o" -ireplace "û","u" -ireplace "ü","u" -ireplace "ÿ","y" -ireplace "ç","c" -ireplace "ñ","n" -replace [regex]::escape('(') -replace [regex]::escape(')') -replace [regex]::escape('[') -replace [regex]::escape(']') -replace [regex]::escape('{') -replace [regex]::escape('}') -replace [regex]::escape('/')
                $OUGroup = $OUGroup.ToUpper()

                if ($OUGroup -ilike "-*")
                {
                    $OUGroup = $OUGroup.Substring(1)
                }
                if ($OUGroup -ilike "*-")
                {
                    $OUGroup = $OUGroup.Substring(0,$OUGroup.Length - 1)
                }

                # Description = CAAF-2021 (18/11/2021)
                $Description = $OUGroup + " (" + $Date + ")"
                
                # Path = OU=CAAF-2021,OU=UTILISATEURS,DC=intra,DC=gobelins,DC=fr
                $Path = "OU=" + $OUGroup + "," + $OUPath + $DC

                # HomeDirectory
                $HomeDrive = $XboxPath + $OUGroup + "\"
                $HomeDirectory = $HomeDrive + $DisplayName

                ### SCRIPTS ###
                if ($OUGroup.Length -gt 1)
                {
                    CSVtoADUser-Groups -OUGroup $OUGroup -Path $Path -XboxPath $XboxPath -HomeDrive $HomeDrive
                }
                #CSVtoADUser-Users -Total $Total -Surname $Surname -GivenName $GivenName -DisplayName $DisplayName -CName $CName -EmailAdress $EmailAdress -SamAccountName $SamAccountName -PrincipalName $PrincipalName -OUGroup $OUGroup -Description $Description -Path $Path -HomeDrive $HomeDrive -HomeDirectory $HomeDirectory -AccountPassword $AccountPassword
            }
        }
    }
    else
    {
        $TxtHost = $CsvPath + " | " + $Error[0] + "`n"
        Write-Warning $TxtHost
    }

    $TxtHost = "Erreur(s) : " + $Error.Count.ToString()
    Write-Host $TxtHost -ForegroundColor Red
    Write-Host $FnTitle -ForegroundColor Cyan
    
    <#
    $Error
    #>
    <#
    Get-Variable
    #>
    <#
    Start-Sleep -Seconds 30
    shutdown -s -t 30
    Start-Sleep -Seconds 3
    exit
    #>
}

function CSVtoADUser-Groups
{
    [CmdletBinding(HelpUri='https://github.com/LesOrcsGithub/GOB-CSVtoADUser')]
    Param(
    [Parameter(Mandatory=$true)][string]$OUGroup,
    [Parameter(Mandatory=$true)][string]$Path,
    [Parameter(Mandatory=$true)][string]$XboxPath,
    [Parameter(Mandatory=$true)][string]$HomeDrive
    )

    $iPath = $Path.Replace("OU=" + $OUGroup + ",","")
    #$iHomeDrive = $HomeDrive.Replace($OUGroup + "\","")
    $ScanOU = Get-ADOrganizationalUnit -Filter * -Properties Name,DistinguishedName | Where-Object {$_.Name -ilike $OUGroup}

    if ($ScanOU -eq $null)
    {
        New-ADOrganizationalUnit -Name $OUGroup -Path $iPath -ProtectedFromAccidentalDeletion $false
    }
}
function CSVtoADUser-Groups_Save
{
    [CmdletBinding(HelpUri='https://github.com/LesOrcsGithub/GOB-CSVtoADUser')]
    Param(
    [Parameter(Mandatory=$true)][string]$OUGroup,
    [Parameter(Mandatory=$true)][string]$Path,
    [Parameter(Mandatory=$true)][string]$HomeDrive
    )

    $iPath = $Path.Replace("OU=" + $OUGroup + ",","")
    $iHomeDrive = $HomeDrive.Replace($OUGroup + "\","")
    $ScanOU = Get-ADOrganizationalUnit -Filter * -Properties Name,DistinguishedName | Where-Object {$_.Name -ilike $OUGroup}
    $ScanXbox = Get-ChildItem -Path "D:\XBOK" -Recurse | Where-Object {$_.Name -ilike "*$OUGroup*"} | Select-Object Name,FullName

    if ($ScanOU -eq $null)
    {
        try
        {
            New-ADOrganizationalUnit -Name $OUGroup -Path $iPath -ProtectedFromAccidentalDeletion $false
            New-ADGroup -Name $OUGroup -Path $Path -GroupScope Global
            New-Item -Path $HomeDrive -ItemType Directory | Out-Null
            CSVtoADUser-Security -HomeDirectory $HomeDrive -SamAccountName $OUGroup -FullControl $false
        }
        catch
        {
            $TxtHost = $OUGroup + " | " + $Error[0] + "`n"
            Write-Warning $TxtHost
            break
        }
    }
    if ($Path -inotlike $ScanOU.DistinguishedName)
    {
        try
        {
            Move-ADObject -Identity $ScanOU.DistinguishedName -TargetPath $iPath
        }
        catch
        {
            $TxtHost = $OUGroup + " | " + $Error[0] + "`n"
            Write-Warning $TxtHost
        }
    }
    if ($ScanXbox.FullName -inotlike $HomeDrive -and $ScanXbox.FullName -ne $null)
    {
        try
        {
            Move-Item -Path $ScanXbox.FullName -Destination $iHomeDrive -Force
        }
        catch
        {
            $TxtHost = $HomeDrive + " | " + $Error[0] + "`n"
            Write-Warning $TxtHost
        }
    }
    elseif ($ScanXbox.FullName -inotlike $HomeDrive -and $ScanXbox.FullName -eq $null)
    {
        try
        {
            New-Item -Path $HomeDrive -ItemType Directory | Out-Null
            CSVtoADUser-Security -HomeDirectory $HomeDrive -SamAccountName $OUGroup -FullControl $false
        }
        catch
        {
            $TxtHost = $HomeDrive + " | " + $Error[0] + "`n"
            Write-Warning $TxtHost
        }
    }
}

function CSVtoADUser-Users
{
    [CmdletBinding(HelpUri='https://github.com/LesOrcsGithub/GOB-CSVtoADUser')]
    Param(
    [Parameter(Mandatory=$false)][int]$Total,
    [Parameter(Mandatory=$false)][string]$Surname,
    [Parameter(Mandatory=$false)][string]$GivenName,
    [Parameter(Mandatory=$false)][string]$DisplayName,
    [Parameter(Mandatory=$false)][string]$CName,
    [Parameter(Mandatory=$false)][string]$EmailAdress,
    [Parameter(Mandatory=$false)][string]$SamAccountName,
    [Parameter(Mandatory=$false)][string]$PrincipalName,
    [Parameter(Mandatory=$false)][string]$OUGroup,
    [Parameter(Mandatory=$false)][string]$Description,
    [Parameter(Mandatory=$false)][string]$Path,
    [Parameter(Mandatory=$false)][string]$HomeDrive,
    [Parameter(Mandatory=$false)][string]$HomeDirectory,
    [Parameter(Mandatory=$false)][string]$AccountPassword
    )

    $Identity = "CN=" + $OUGroup + "," + $Path
    $Members = "CN=" + $CName + "," + $Path
    $ScanADUser = Get-ADUser -Filter * -Properties CN,DistinguishedName,HomeDirectory,SamAccountName | Where-Object {$_.CN -ilike $CName -or $_.SamAccountName -ilike $SamAccountName}

    if ($ScanADUser -eq $null)
    {
        try
        {
            New-ADUser -Surname $Surname -GivenName $GivenName -Name $CName -DisplayName $DisplayName -SamAccountName $SamAccountName -UserPrincipalName $PrincipalName -EmailAddress $EmailAdress -Description $Description -Path $Path -HomeDrive "U:" -HomeDirectory $HomeDirectory -AccountPassword (ConvertTo-SecureString $AccountPassword -AsPlainText -Force) -PasswordNeverExpires $false -ChangePasswordAtLogon $true -Enabled $true
            Add-ADGroupMember -Identity $Identity -Members $Members
            New-Item -Path $HomeDirectory -ItemType Directory -Force | Out-Null
            CSVtoADUser-Security -HomeDirectory $HomeDirectory -SamAccountName $SamAccountName
            CSVtoADUser-Txt -Total $Total -CName $CName -SamAccountName $SamAccountName -OUGroup $OUGroup -Color "Green"
        }
        catch
        {
            $TxtHost = $CName + " | " + $Error[0] + "`n"
            Write-Warning $TxtHost
        }
    }
    else
    {
        try
        {
            if ($GivenName -gt 1)
            {
                Set-ADUser -Identity $ScanADUser.DistinguishedName -Surname $Surname -GivenName $GivenName -DisplayName $DisplayName -SamAccountName $SamAccountName -UserPrincipalName $PrincipalName -EmailAddress $EmailAdress -Description $Description -HomeDrive "U:" -HomeDirectory $HomeDirectory -PasswordNeverExpires $false -ChangePasswordAtLogon $true -Enabled $true
            }
            else
            {
                Set-ADUser -Identity $ScanADUser.DistinguishedName -Surname $Surname -GivenName $null -DisplayName $DisplayName -SamAccountName $SamAccountName -UserPrincipalName $PrincipalName -EmailAddress $EmailAdress -Description $Description -HomeDrive "U:" -HomeDirectory $HomeDirectory -PasswordNeverExpires $false -ChangePasswordAtLogon $true -Enabled $true
            }
            Move-ADObject -Identity $ScanADUser.DistinguishedName -TargetPath $Path
            try
            {
                Move-Item -Path $ScanADUser.HomeDirectory -Destination $HomeDrive -PassThru
            }
            catch
            {
                $TxtHost = $CName + " | " + $Error[0] + "`n"
                Write-Warning $TxtHost
            }
            CSVtoADUser-Txt -Total $Total -CName $CName -SamAccountName $SamAccountName -OUGroup $OUGroup -Color "Yellow"
        }
        catch
        {
            $TxtHost = $CName + " | " + $Error[0] + "`n"
            Write-Warning $TxtHost
        }
    }
}

function CSVtoADUser-Security
{
    [CmdletBinding(HelpUri='https://github.com/LesOrcsGithub/GOB-CSVtoADUser')]
    Param(
    [Parameter(Mandatory=$true)][string]$HomeDirectory,
    [Parameter(Mandatory=$true)][string]$SamAccountName,
    [Parameter(Mandatory=$true)][bool]$FullControl
    )

    if ($FullControl -eq $true)
    {
        $Acl = Get-Acl -Path $HomeDirectory
        $Permission = $SamAccountName, 'FullControl', 'ContainerInherit, ObjectInherit', 'None', 'Allow' 
        $Rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $Permission
        $Acl.SetAccessRule($Rule)
        $Acl | Set-Acl -Path $HomeDirectory
    }
    else
    {
        $Acl = Get-Acl -Path $HomeDirectory
        $Permission = $SamAccountName, 'Read,Modify', 'ContainerInherit, ObjectInherit', 'None', 'Allow' 
        $Rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $Permission
        $Acl.SetAccessRule($Rule)
        $Acl | Set-Acl -Path $HomeDirectory
    }
}

function CSVtoADUser-Txt
{
    [CmdletBinding(HelpUri='https://github.com/LesOrcsGithub/GOB-CSVtoADUser')]
    Param(
    [Parameter(Mandatory=$true)][int]$Total,
    [Parameter(Mandatory=$true)][string]$CName,
    [Parameter(Mandatory=$true)][string]$SamAccountName,
    [Parameter(Mandatory=$true)][string]$OUGroup,
    [Parameter(Mandatory=$true)][string]$Color
    )

    $TxtHost = $Total.ToString() + ". " + $CName
    Write-Host $TxtHost -ForegroundColor $Color
    while ($Tic -ne $Total.ToString().Length)
    {
        $Tic++
        $Toc = $Toc + "-"
    }
    $TxtHost = "|" + $Toc + " " + $OUGroup + "/" + $SamAccountName + "`n"
    Write-Host $TxtHost -ForegroundColor White
}
